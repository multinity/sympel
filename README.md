# sympel (v1.2)
Sympel adalah sebuah Content Management System(CMS) sederhana yang dibuat untuk web dengan satu halaman. Tujuan utama dibuatnya CMS ini untuk dipelajari atau memperkenalkan konsep dasar dari suatu CMS.

## Detail
- Responsive Design
- Easy to use
- Easy to develop
- Use PHP Native non OOP
- CKEditor
- Theme Editor
- and others ...

## Required
- PHP 5.3+
- MySQL 5.1+
- PHP Mail Function

## Instalasi
1. Buat satu buah database MySQL
2. Buka halaman instalasi *sympel*
    - Baca lisensi terlebih dahulu
    - Klik `Pasang sympel`
    - Isi semua kolom sesuai dengan informasi database anda (Host, Database, Username, Password)
    - Kemudian, sesuaikan informasi situs yang akan anda buat
    - Setelah itu isi rincian akun admin anda
3. Selesai, hapus folder `pasang`

## Instalasi Manual
1. Buat satu buah database
2. Import file `pasang/sql/sympel.sql` ke database yang tadi dibuat
3. Pada folder `sympel` rename `db-sample.php` menjadi `db.php`
4. Isi detail database pada file `db.php`
5. Kemudian masuk kehalaman instalasi *sympel* untuk mengatur informasi website dan administrator
6. Selesai, hapus folder `pasang`

## Preview
### Login Page
[![admin login page][1]][1]
### Dashboard
[![dashboard][2]][1]
### Mobile
[![Mobile][3]][1]
### Mobile #2
[![Mobile #2][4]][1]

  [1]: http://i.stack.imgur.com/Tczva.png
  [2]: http://i.imgur.com/vpnDmy8.png
  [3]: http://i.imgur.com/JcXUm0u.png
  [4]: http://i.imgur.com/GrPSDYt.png
  
## License
[Click Me!](https://github.com/multinity/sympel/blob/master/LICENSE)
