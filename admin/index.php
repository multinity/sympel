<?php
/**
 * Sympel (CMS)
 *
 * Sympel adalah sebuah Content Management System(CMS) sederhana
 * yang dibuat untuk web dengan satu halaman. Tujuan utama
 * dibuatnya CMS ini untuk dipelajari dan dikembangkan kembali.
 *
 * @package  Sympel
 * @author   Muhamad Nauval Azhar
 * @copyright	Copyright (c) 2014 - 2016, Muhamad Nauval Azhar (http://nauvalazhar.net/)
 * @license	http://www.gnu.org/licenses/gpl-3.0-standalone.html GNU General Public License v3
 * @link	https://multinity.github.io/sympel/
 * @since	Version 1.0.0
 * 
 */

ob_start();
session_start();
include("../config.php");

if(!file_exists("../db.php")){
	header("Location: $dir[install]/index.php");
}else{
	if(!$session['user_adm']) {
		include("{$site['login']}");
	}else{
		include("main.php");
	}
}
?>